访问[GitLab注册页面](https://gitlab.com/users/sign_up)，输入姓名、用户名、邮箱地址和密码，点击蓝色按钮“Redister”。

> GitLab的管理员可以设置在用户注册时是否需要验证邮箱。如果开启邮箱验证，用户提供的邮箱内会接收到注册确认邮件，点击邮件中的链接才能完成注册。

访问[GitLab登录页面](https://gitlab.com/users/sign_in)，输入用户名/邮箱地址和密码完成登录。

如果忘记密码，可以在GitLab登录页面点击“Forget your password?”。在新的页面中输入注册邮箱地址，根据提示设置新的密码。

<figure>
    <img src="pic/gitlab-signup.png" alt="Trulli" style="width:45%">
    <img src="pic/gitlab-signin.png" alt="Trulli" style="width:45%">
    <figcaption align = "center">
        <b>Figure 2-1. GitLab 注册和登录页面</b>
    </figcaption>
</figure>